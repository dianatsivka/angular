import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class LoginService {
  constructor(private http: HttpClient) { }

  logIn = (user: object) => {
    this.http.post('http://localhost:8080/user/login', user)
    .subscribe((res) => {
      let Token = Object.values(res)[1]
      if(Token){
        sessionStorage.setItem('token', Token)
      }
    });
  }
  
}