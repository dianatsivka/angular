interface Board {
  name: string;
  description: string;
}

export {
  Board
}