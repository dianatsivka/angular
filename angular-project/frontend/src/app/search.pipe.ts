import { Pipe, PipeTransform } from '@angular/core';
@Pipe({
    name:'search'
})
export class SearchPipe implements PipeTransform {
    transform(boardsArr: string[], searchInput: string): any[]{
      if(!searchInput){
        return boardsArr
      }
      searchInput = searchInput.toLocaleLowerCase()
      return boardsArr.filter((board:any) => {
        return board.name.includes(searchInput)
       })
     }
}