import { Component, OnInit } from '@angular/core';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-add-task',
  templateUrl: './add-task.component.html',
  styleUrls: ['./add-task.component.css']
})
export class AddTaskComponent implements OnInit {
  faPlus = faPlus

  constructor(private router: Router, private http: HttpClient, private route: ActivatedRoute) { }

  hidden = false;
  board: string = ''

  hide = () => {
    return this.hidden = !this.hidden
  }

  taskPost = () => {
    let name = ((document.getElementById("task-name") as HTMLInputElement)).value;
    let comment = ((document.getElementById("comment") as HTMLInputElement)).value;
    let task = {name, comment}
    console.log(task)
    this.http.post(`http://localhost:8080/board/${this.board}/task`, task)
    .subscribe((res) => {
      location.reload()
    });
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.board = params['id']
   });
  }
}
