import { Component, OnInit } from '@angular/core';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-board',
  templateUrl: './add-board.component.html',
  styleUrls: ['./add-board.component.css']
})
export class AddBoardComponent implements OnInit {

  faPlus = faPlus

  constructor(private router: Router, private http: HttpClient) { }

  hidden = false;

  hide = () => {
    return this.hidden = !this.hidden
  }

  boardPost = () => {
    let name = ((document.getElementById("name") as HTMLInputElement)).value;
    let description = ((document.getElementById("description") as HTMLInputElement)).value;
    let board = {name, description}
    this.http.post('http://localhost:8080/board', board)
    .subscribe((res) => {
      this.router.navigate([`/board/${Object.values(res)[4]}`])
      console.log(Object.values(res)[4])
    });
  }

  ngOnInit(): void {
  }

}
