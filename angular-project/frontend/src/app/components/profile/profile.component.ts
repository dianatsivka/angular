import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  constructor(private http: HttpClient) { }

  result: any;
  user: any;
  password: boolean = false;

  ngOnInit(): void {
    this.http.get('http://localhost:8080/user/me')
    .subscribe((res) => {
      this.result = res
      console.log(res)
      this.user = this.result.user.username
    });
  }

  deleteUser = () => {
    this.http.delete('http://localhost:8080/user/me')
    .subscribe((res) => {
      console.log(res)
    });
  }
  changeUser = () => {
    this.password = !this.password
  }
  submit = () => {
    let oldPassword = (document.getElementById('oldP') as HTMLInputElement).value
    let newPassword = (document.getElementById('newP') as HTMLInputElement).value
    if( oldPassword !== '' && newPassword !== '' ){
      this.http.patch('http://localhost:8080/user/me', {oldPassword, newPassword})
      .subscribe((res) => {
        console.log(res)
      });
    }
    this.password = !this.password
  }

}
