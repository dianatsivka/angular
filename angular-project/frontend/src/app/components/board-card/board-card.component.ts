import { Component, OnInit, Input } from '@angular/core';
import { faEllipsisVertical, faSave } from '@fortawesome/free-solid-svg-icons';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-board-card',
  templateUrl: './board-card.component.html',
  styleUrls: ['./board-card.component.css']
})
export class BoardCardComponent implements OnInit {

  faEllipsisVertical = faEllipsisVertical;

  constructor( private router: Router, private http: HttpClient) { }

  editDelete = false;
  edit = false;
  @Input() board!: any;

  ngOnInit(): void {
  }

  getBoard = (id: string) =>{
    //console.log(id)
    this.router.navigate([`/board/${id}`])
  }

  more = () => {
    this.editDelete = !this.editDelete
  }

  editFunc = (id:string) => {
    this.editDelete = !this.editDelete
    this.edit = !this.edit
  }
  save = (id:string) => {
    let name = (document.getElementById('board-name') as HTMLInputElement).value
    if(name && name != ''){
      this.http.put(`http://localhost:8080/board/${id}`, {name})
    .subscribe((res) => {
console.log(res)
    });
    this.edit = !this.edit
    }else{
      alert('Something went wrong')
      this.edit = !this.edit
    }
  }
  delete = (id:string) => {
    console.log(id)
    this.http.delete(`http://localhost:8080/board/${id}`)
    .subscribe((res) => {
console.log(res)
    });
  }

}
