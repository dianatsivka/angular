import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/services/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private http: HttpClient, private router: Router, private auth: LoginService) { }

  signIn = () => {
    let username = ((document.getElementById("username") as HTMLInputElement)).value;
    let password = ((document.getElementById("password") as HTMLInputElement)).value;
    let user = {username, password}
    this.auth.logIn(user)
    this.router.navigate(['/dashboard'])
  }

  ngOnInit(): void {
  }

}
