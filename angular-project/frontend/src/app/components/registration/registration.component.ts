import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  constructor(private http: HttpClient, private router: Router) { }

  submit = () => {
    let username = ((document.getElementById("username") as HTMLInputElement)).value;
    let password = ((document.getElementById("password") as HTMLInputElement)).value;
    let password2 = ((document.getElementById("password2") as HTMLInputElement)).value;
    let user = {username, password, password2}
    this.http.post('http://localhost:8080/user/register', user)
    .subscribe((res) => {
      console.log(res)
    });
    this.router.navigate(['/login'])
  }

  ngOnInit(): void {

  }

}
