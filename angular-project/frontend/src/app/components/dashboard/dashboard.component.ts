import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { __values } from 'tslib';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor(private http: HttpClient, private router: Router) { }

  result: any;
  boards: any;
  boardsArr: any;
  user: any;
  select: any;
  searchInput: any;

  ngOnInit(): void {
    this.http.get('http://localhost:8080/board')
    .subscribe((res) => {
      this.result = res
      this.boards = this.result.boards
      sessionStorage.setItem('boards', JSON.stringify(this.boards))
      console.log(this.boards)
    });
    this.boardsArr = JSON.parse(sessionStorage.getItem('boards') || '{}')
    this.http.get('http://localhost:8080/user/me')
    .subscribe((res) => {
      this.result = res
      this.user = this.result.user.username
    });
    this.select = document.getElementById('select')
  }
  asc = () => {
    if(this.select.selectedIndex === 2){
      this.boardsArr.sort((a:any, b:any) => a.name.localeCompare(b.name))
    }
    if(this.select.selectedIndex === 1){
      this.boardsArr.sort((a:any, b:any) => a.description.localeCompare(b.description))
    }
    if(this.select.selectedIndex === 0){
      this.boardsArr.sort((a:any, b:any) => a.createdAt.localeCompare(b.createdAt))
    }
  }
  desc = () => {
    if(this.select.selectedIndex === 2){
      this.boardsArr.sort((a:any, b:any) => b.name.localeCompare(a.name))
    }
    if(this.select.selectedIndex === 1){
      this.boardsArr.sort((a:any, b:any) => b.description.localeCompare(a.description))
    }
    if(this.select.selectedIndex === 0){
      this.boardsArr.sort((a:any, b:any) => b.createdAt.localeCompare(a.createdAt))
    }
  }
}
