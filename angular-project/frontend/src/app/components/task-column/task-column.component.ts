import { Component, OnInit, Input} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from  '@angular/common/http';
import { faPen, faTrash, faBoxArchive } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-task-column',
  templateUrl: './task-column.component.html',
  styleUrls: ['./task-column.component.css']
})
export class TaskColumnComponent implements OnInit {

  faPen = faPen
  faTrash = faTrash
  faBoxArchive = faBoxArchive

  board: string = '';
  result: any;
  tasks: any;
  drag: any;
  drop: any;
  dropzone: any;
  toDo : any[] = [];
  progress : any[] = [];
  done : any[] = [];
  draggableElement: any;
  draggableId: any;
  dropId:string = '';
  startDrag: any;
  index: number = 0;

  constructor(private route: ActivatedRoute, private http:HttpClient) { }

  onDragStart = (task: any, event: Event) => {
    this.startDrag = event.composedPath()[1]
    console.log(task)
    this.draggableElement = task
    this.draggableId = task._id
  }
  onDragOver = (event: Event) => {
    event.preventDefault();
  }
  onDrop = (event: Event) => {
    console.log(event)
    this.drop = event.target
    this.dropId = this.drop.id
    this.dropzone = document.getElementById(this.drop.id);
    if(this.dropId == 'progress'){
      this.progress.push(this.draggableElement)
      this.http.patch(`http://localhost:8080/board/${this.board}/task/${this.draggableId}`, { status: 'In progress' })
      .subscribe((res) => {
        console.log(res)
    });
    }
    if(this.dropId == 'toDo'){
      this.toDo.push(this.draggableElement)
      this.http.patch(`http://localhost:8080/board/${this.board}/task/${this.draggableId}`, { status: 'To do' })
      .subscribe((res) => {
        console.log(res)
    });
    }
    if(this.dropId == 'done'){
      this.done.push(this.draggableElement)
      this.http.patch(`http://localhost:8080/board/${this.board}/task/${this.draggableId}`, { status: 'Done' })
      .subscribe((res) => {
        console.log(res)
    });
    }
    if(this.startDrag.id == 'toDo'){
      this.index = this.toDo.indexOf(this.draggableElement)
      this.toDo.splice(this.index,this.index + 1)
    }
    if(this.startDrag.id == 'progress'){
      this.index = this.progress.indexOf(this.draggableElement)
      this.progress.splice(this.index,this.index + 1)
    }
    if(this.startDrag.id == 'done'){
      this.index = this.done.indexOf(this.draggableElement)
      this.done.splice(this.index,this.index + 1)
    }
  }

  delete = (id: string) => {
    this.http.delete(`http://localhost:8080/board/${this.board}/task/${id}`)
    .subscribe((res) => {
      location.reload()
    })
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.board = params['id']
   });

   this.http.get<object>(`http://localhost:8080/board/${this.board}/task`)
   .subscribe((res) => {
     this.result = res
     this.tasks = this.result.tasks
     for( let task of this.tasks){
       if( task.status === 'To do' ){
          this.toDo.push(task)
       }
       if( task.status === 'In progress' ){
         this.progress.push(task)
       }
       if( task.status === 'Done' ){
         this.done.push(task)
       }
     }
 });
}
}

