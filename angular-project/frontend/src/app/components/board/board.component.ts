import { Component, OnInit, Output } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from  '@angular/common/http';
import { Board } from 'src/app/interfaces/interface';

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.css']
})
export class BoardComponent implements OnInit {

  constructor(private route: ActivatedRoute, private http:HttpClient) { }

  board = '';
  result :any;
  info: any;

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.board = params['id']
   });

   this.http.get(`http://localhost:8080/board/${this.board}`)
    .subscribe((res) => {
      this.result = res
      this.info = {
        name: this.result.Board.name,
        description: this.result.Board.description}
  });
}
}
