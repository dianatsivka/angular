const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors')


const app = express();

mongoose.connect('mongodb+srv://dianatsivka:epamlab2022@taskmanager.dhafjjj.mongodb.net/?retryWrites=true&w=majority')
.then(() => {
  console.log('MongoDB connected')
})
.catch(err => {
  console.log(err)
})

app.use(express.json());
app.use(cors())

const { userRoutes } = require('./routes/userRoutes');
app.use('/user', userRoutes);
const { boardRoutes } = require('./routes/boardRoutes');
app.use('/board', boardRoutes);
const { taskRoutes } = require('./routes/taskRoutes');
app.use('/board', taskRoutes);


const PORT = process.env.PORT || 8080;
app.listen(PORT, console.log(`Server is running on port ${PORT}`));