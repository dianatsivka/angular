const express = require('express');
const { User } = require('../models/User');
const router = express.Router();
const { registerUser, loginUser, changePassword, getProfileInfo, deleteProfile } = require('../services/userServices');
const { authMiddleware } = require('../authMiddleware');


router.post('/register', registerUser)

router.post('/login', loginUser)

router.patch('/me', authMiddleware, changePassword)

router.get('/me', authMiddleware, getProfileInfo)

router.delete('/me', authMiddleware, deleteProfile)

module.exports = {
  userRoutes: router,
};