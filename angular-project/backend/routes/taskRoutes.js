const { Task } = require('../models/Task');
const express = require('express');
const router = express.Router();
const { createTask, getAllTasks, getSingleTask, updateTask, deleteTask } = require('../services/taskServices');


router.post('/:boardId/task', createTask)

router.get('/:boardId/task', getAllTasks)

router.get('/:boardId/task/:id', getSingleTask)

router.patch('/:boardId/task/:id', updateTask)

router.delete('/:boardId/task/:id', deleteTask)

module.exports = {
  taskRoutes: router,
};