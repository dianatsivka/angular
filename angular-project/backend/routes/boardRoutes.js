const { Board } = require('../models/Board');
const express = require('express');
const router = express.Router();
const { createBoard, getAllBoards, getSingleBoard, updateBoard, deleteBoard } = require('../services/boardServices');
const { authMiddleware } = require('../authMiddleware');


router.post('/',authMiddleware, createBoard)

router.get('/',authMiddleware, getAllBoards)

router.get('/:id', getSingleBoard)

router.put('/:id', updateBoard)

router.delete('/:id', deleteBoard)

module.exports = {
  boardRoutes: router,
};