const { Task } = require('../models/Task');

const createTask = (req, res, next) => {
  try {
  const task = new Task({
    name: req.body.name,
    boardId: req.params.boardId,
    comments: req.body.comments,
    status: req.body.status,
    createdAt: req.body.date,
    archived: req.body.archived
  });

  task.save().then((saved) => {
    res.json({message: saved});
  });
  } catch (error) {
    res.status(404).json('Server error createTask')
  }
}

const getAllTasks = (req, res, next) => {
  try {
  Task.find({boardId: req.params.boardId}).then((result) => {
    res.json({tasks: result});
  });
  } catch (error) {
    res.status(404).json('Server error getAllTask')
  }
}

const getSingleTask = (req, res, next) => Task.findById(req.params.id)
.then((task) => {
  res.json({task: task});
});

const updateTask = (req, res, next) => {
  const { name, comments, status, archived } = req.body;
  return Task.findByIdAndUpdate(req.params.id, {$set: { name, comments, status, archived } })
    .then((task) => {
      res.json({message: 'updated'});
    });
};

const deleteTask = (req, res, next) => Task.findByIdAndDelete(req.params.id)
  .then((task) => {
    res.json({message: 'deleted Task'});
  });

module.exports = {
  createTask,
  getAllTasks,
  getSingleTask,
  updateTask,
  deleteTask
};