const { Board } = require('../models/Board');
const jwt = require('jsonwebtoken');

const createBoard = (req, res, next) => {
  try {
  const board = new Board({
    name: req.body.name,
    userId: req.user.userId,
    description: req.body.description,
    createdAt: req.body.date
  });
  const payload = { name: board.name, boardId: board._id };
  const jwt_token = jwt.sign(payload, 'secret-jwt-key');

  board.save().then((saved) => {
    res.send(saved);
  });
  } catch (error) {
    res.status(404).json('Server error createBoard')
  }
}

const getAllBoards = (req, res, next) => {
  try {
  Board.find({borderId: req.user.userId}).then((result) => {
    res.json({boards: result});
  });
  } catch (error) {
    res.status(404).json('Server error getAllBoard')
  }
}

const getSingleBoard = (req, res, next) => Board.findById(req.params.id)
.then((board) => {
  res.json({Board: board});
});

const updateBoard = (req, res, next) => {
  const { name} = req.body;
  return Board.findByIdAndUpdate(req.params.id, {$set: { name } })
    .then((board) => {
      res.json({message: 'updated'});
    });
};

const deleteBoard = (req, res, next) => Board.findByIdAndDelete(req.params.id)
  .then((board) => {
    res.json({message: 'deleted Board'});
  });


module.exports = {
  createBoard,
  getAllBoards,
  getSingleBoard,
  updateBoard,
  deleteBoard
};