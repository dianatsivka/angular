const mongoose = require('mongoose');

const Board = mongoose.model('boards', {
  name: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  createdAt: {
    type: Date,
    default: new Date()
  },
  userId:{
    type: String,
  }
});

module.exports = {
  Board
};