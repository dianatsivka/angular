const mongoose = require('mongoose');

const User = mongoose.model('user', {
  username: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
  createdAt: {
    type: Date,
    default: new Date()
  },
});

module.exports = {
  User
};