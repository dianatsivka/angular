const mongoose = require('mongoose');

const Task = mongoose.model('tasks', {
  name: {
    type: String,
    required: true,
  },
  comments: {
    type: String,
    required: false,
  },
  status: {
    type: String,
    required: false,
    default: 'To do'
  },
  boardId: {
    type: String,
  },
  createdAt: {
    type: Date,
    default: new Date()
  },
  archived: {
    type: Boolean,
    default: false
  }
});

module.exports = {
  Task
};